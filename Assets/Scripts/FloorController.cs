﻿using UnityEngine;

public class FloorController : MonoBehaviour
{
    private GameMaster _gameMaster;
    private BallsPoolMaster _ballsPoolMaster;

    private const string BRICK_TAG = "Brick";
    private const string BALL_TAG = "Ball";

    private void Awake()
    {
        _gameMaster = FindObjectOfType<GameMaster>();
        _ballsPoolMaster = FindObjectOfType<BallsPoolMaster>();
    }


    private void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case BRICK_TAG:
                _gameMaster.ObjectFell();
                Destroy(other.gameObject);
                break;
            case BALL_TAG:
                _ballsPoolMaster.InsertBall(other.gameObject);
                break;
        }
    }
}
