﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{
    public int m_level;
    public Transform m_objectsRoot;

    public int m_ballsAmount { get; private set; }
    public int m_nextLevelId => m_level == LAST_LEVEL ? 0 : (m_level + 1);

    public event UnityAction<int> ShotEventHandler;
    public event UnityAction<float> ProgressUpdateEventHandler; 
    
    private const int LAST_LEVEL = 9;
    private const float WAIT_TO_DIE = 3f;
    private int _activeBricksCount;
    private int _initBricksCount;
    
    private void Awake()
    {
        AnalyticsMaster.FbInit();
        m_ballsAmount = (m_level + 1) * 3;
        _initBricksCount = _activeBricksCount = m_objectsRoot.childCount;
    }
    
    public void ObjectFell()
    {
        --_activeBricksCount;
        ProgressUpdateEventHandler?.Invoke(1f - (float)_activeBricksCount / _initBricksCount);
        if (_activeBricksCount != 0)
        {
            return;
        }

        SceneManager.LoadScene($"Level_{m_nextLevelId}");
        AnalyticsMaster.LogEvent("LevelComplete", 0f, new Dictionary<string, object>{{"Level", m_level}});
    }

    public void OnShot()
    {
        --m_ballsAmount;
        ShotEventHandler?.Invoke(m_ballsAmount);
        if (m_ballsAmount == 0)
        {
            StartCoroutine(LostCoroutine());
        }
    }



    private IEnumerator LostCoroutine()
    {
        yield return new WaitForSeconds(WAIT_TO_DIE);
        SceneManager.LoadScene($"Level_{m_level}");
    }


}
