﻿using UnityEngine;
using UnityEngine.UI;

public class HUDMaster : MonoBehaviour
{
    public Text _currentLevelTextField;
    public Text _nextLevelTextField;
    public Text _bulletTextField;
    public Slider _levelCompleteSlider;
    
    private GameMaster _gameMaster;

    private void Awake()
    {
        _gameMaster = FindObjectOfType<GameMaster>();
    }

    private void Start()
    {
        _currentLevelTextField.text = $"Level {_gameMaster.m_level + 1}";
        _nextLevelTextField.text = $"Level {_gameMaster.m_nextLevelId + 1}";
        _levelCompleteSlider.value = 0f;
        _bulletTextField.text = $"Balls amount {_gameMaster.m_ballsAmount}";
    }

    private void OnEnable()
    {
        _gameMaster.ShotEventHandler += OnShot;
        _gameMaster.ProgressUpdateEventHandler += OnProgressUpdate;
    }

    private void OnDisable()
    {
        _gameMaster.ShotEventHandler -= OnShot;
        _gameMaster.ProgressUpdateEventHandler -= OnProgressUpdate;
    }

    private void OnShot(int amount)
    {
        _bulletTextField.text = $"Balls amount {amount}";
    }

    private void OnProgressUpdate(float progress)
    {
        _levelCompleteSlider.value = progress;
    }
}
