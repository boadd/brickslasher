﻿using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public Transform m_spawnPoint;
    public Transform m_gunObject;
    public float m_aimingRange = 5f;
    public float m_strikeForce;
    
    private GameMaster _gameMaster;
    private BallsPoolMaster _ballsPoolMaster;
    private Camera _mainCamera;


    private void Awake()
    {
        _mainCamera = Camera.main;
        _gameMaster = FindObjectOfType<GameMaster>();
        _ballsPoolMaster = FindObjectOfType<BallsPoolMaster>();
    }


    private void Update()
    {
        if (_gameMaster.m_ballsAmount == 0)
        {
            return;
        }
        
        if (Input.GetMouseButtonDown(0))
        {
            _gameMaster.OnShot();
            
            var ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
            m_gunObject.LookAt(ray.GetPoint(m_aimingRange));
            var ball = _ballsPoolMaster.GetBall();
            ball.transform.position = m_spawnPoint.position;
            var ballRigidbody = ball.GetComponent<Rigidbody>();
            ballRigidbody.velocity = m_gunObject.forward * m_strikeForce;
        }
    }
    
}
