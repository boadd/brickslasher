﻿using System.Collections.Generic;
using UnityEngine;

public class BallsPoolMaster : MonoBehaviour
{
    public GameObject m_ball;
    private readonly Stack<GameObject> _ballsPool = new Stack<GameObject>();
    
    public GameObject GetBall()
    {
        if (_ballsPool.Count == 0)
        {
            FillPool(5);
        }

        var ball = _ballsPool.Pop();
        ball.SetActive(true);
        return ball;
    }

    public void InsertBall(GameObject ball)
    {
        ball.SetActive(false);
        _ballsPool.Push(ball);
    }

    private void FillPool(int count)
    {
        for (int i = 0; i < count; ++i)
        {
            var ball = Instantiate(m_ball);
            ball.SetActive(false);
            _ballsPool.Push(ball);
        }
    }

    private void Start()
    {
        FillPool(10);
    }
}
