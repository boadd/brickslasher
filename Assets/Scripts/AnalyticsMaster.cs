﻿using System.Collections.Generic;
using Facebook.Unity;

public static class AnalyticsMaster
{
    public static void FbInit()
    {
        if (FB.IsInitialized)
        {
            return;
        }
        FB.Init();
    }


    public static void LogEvent(string logEvent, float valueToSum = 0, Dictionary<string, object> parameters = null)
    {
        FbInit();
        FB.LogAppEvent(logEvent, valueToSum, parameters);
    }
    
}
